const express = require("express");
const app = express();
const { users, match, history } = require("./models");

app.use(express.urlencoded({ extended: false }));
app.set("view engine", "ejs");
app.use(express.static("assets"));

app.get("/users/create", (req, res) => {
  res.render("users/form");
});

app.post("/users", (req, res) => {
  users
    .create({
      name: req.body.name,
      username: req.body.username,
      email: req.body.email,
      password: req.body.password,
      address: req.body.address,
    })
    .then(function (user) {
      res.send(`Berhasil tambah users ${user.name}`);
    });
});

app.get("/users/list", (req, res) => {
  users.findAll().then((user) => {
    res.render("users/list", { user });
  });
});

// app.get("/users/list", (req, res) => {
//   users
//     .findAll({
//       include: match,
//     })
//     .then((user) => {
//       res.render("users/list", { user });
//     });
// });

app.get("/users/view/:id", (req, res) => {
  users
    .findOne({
      where: { id: req.params.id },
    })
    .then((user) => {
      if (!user) {
        res.status(404).send("Not found");
        return;
      }

      res.render("users/view", { user });
    });
});

app.get("/users/update/:id", (req, res) => {
  users
    .findOne({
      where: { id: req.params.id },
    })
    .then((user) => {
      if (!user) {
        res.status(404).send("Not found");
        return;
      }

      res.render("users/update", { user });
    });
});

app.post("/users/update", (req, res) => {
  users
    .update(
      {
        name: req.body.name,
        username: req.body.username,
        email: req.body.email,
        password: req.body.password,
        address: req.body.address,
      },
      {
        where: { id: req.body.id },
      }
    )
    .then(function (user) {
      res.redirect("/users/list");
    });
});

app.get("/users/delete/:id", (req, res) => {
  users
    .destroy({
      where: { id: req.params.id },
    })
    .then(function () {
      res.redirect("/users/list");
    });
});

app.get("/dashboard/login", (req, res) => {
  res.render("login");
});

app.post("/dashboard", (req, res) => {
  let username = req.body.username;
  let password = req.body.password;

  if (username == "admin" && password == "123456") {
    res.redirect("users/list");
  }

  res.redirect("/dashboard/login");
});

app.get("/match/create", (req, res) => {
  res.render("match/form");
});

app.post("/match", (req, res) => {
  match
    .create({
      player_1_id: req.body.player_1_id,
      player_2_id: req.body.player_2_id,
      player_1_hand: req.body.player_1_hand,
      player_2_hand: req.body.player_2_hand,
      result: req.body.result,
    })
    .then(function (matches) {
      res.redirect("/match/list");
    });
});

app.get("/match/list", (req, res) => {
  match.findAll().then((matches) => {
    res.render("match/list", { matches });
  });
});

app.get("/match/view/:id", (req, res) => {
  match
    .findOne({
      where: { id: req.params.id },
    })
    .then((matches) => {
      if (!matches) {
        res.status(404).send("Not found");
        return;
      }

      res.render("match/view", { matches });
    });
});

app.get("/match/update/:id", (req, res) => {
  match
    .findOne({
      where: { id: req.params.id },
    })
    .then((matches) => {
      if (!matches) {
        res.status(404).send("Not found");
        return;
      }

      res.render("match/update", { matches });
    });
});

app.post("/match/update", (req, res) => {
  match
    .update(
      {
        player_1_id: req.body.player_1_id,
        player_2_id: req.body.player_2_id,
        player_1_hand: req.body.player_1_hand,
        player_2_hand: req.body.player_2_hand,
        result: req.body.result,
      },
      {
        where: { id: req.body.id },
      }
    )
    .then(function (match) {
      res.redirect("/match/list");
    });
});

app.get("/match/delete/:id", (req, res) => {
  match
    .destroy({
      where: { id: req.params.id },
    })
    .then(function () {
      res.redirect("/match/list");
    });
});

app.get("/history/create", (req, res) => {
  res.render("history/form");
});

app.post("/history", (req, res) => {
  history
    .create({
      player_win_id: req.body.player_win_id,
      player_lose_id: req.body.player_lose_id,
      date: req.body.date,
      time: req.body.time,
      score: req.body.score,
    })
    .then(function (histories) {
      res.redirect("/history/list");
    });
});

app.get("/history/list", (req, res) => {
  history.findAll().then((histories) => {
    res.render("history/list", { histories });
  });
});

app.get("/history/view/:id", (req, res) => {
  history
    .findOne({
      where: { id: req.params.id },
    })
    .then((histories) => {
      if (!histories) {
        res.status(404).send("Not found");
        return;
      }

      res.render("history/view", { histories });
    });
});

app.get("/history/update/:id", (req, res) => {
  history
    .findOne({
      where: { id: req.params.id },
    })
    .then((histories) => {
      if (!histories) {
        res.status(404).send("Not found");
        return;
      }

      res.render("history/update", { histories });
    });
});

app.post("/history/update", (req, res) => {
  history
    .update(
      {
        player_win_id: req.body.player_win_id,
        player_lose_id: req.body.player_lose_id,
        date: req.body.date,
        time: req.body.time,
        score: req.body.score,
      },
      {
        where: { id: req.body.id },
      }
    )
    .then(function (history) {
      res.redirect("/history/list");
    });
});

app.get("/history/delete/:id", (req, res) => {
  history
    .destroy({
      where: { id: req.params.id },
    })
    .then(function () {
      res.redirect("/history/list");
    });
});

app.listen(8000, () => console.log(`Web App Up in http://localhost:8000`));
