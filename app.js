const express = require("express");
const app = express();
const port = 8000;
const router = require("./assets/js");
const { users } = require("./models");

app.set("view engine", "ejs");
app.use(express.static("assets"));
app.use(express.urlencoded({ extended: false }));

app.use(express.json());

app.get("/users", (req, res) => {
  users.findAll().then((user) => {
    res.status(200).json(user);
  });
});

app.get("/users/:id", (req, res) => {
  users
    .findOne({
      where: { id: req.params.id },
    })
    .then((user) => {
      if (!user) {
        res.status(404).send("Not found");
        return;
      }
      res.status(200).json(user);
    });
});

app.post("/users", (req, res) => {
  //TODO add validation
  users
    .create({
      name: req.body.name,
      username: req.body.username,
      email: req.body.email,
      password: req.body.password,
      address: req.body.address,
    })
    .then(function (user) {
      res.status(201).json(user);
    })
    .catch((err) => res.status(500).send(err));
});

app.put("/users/:id", (req, res) => {
  //TODO add validation
  users
    .update(
      {
        name: req.body.name,
        username: req.body.username,
        email: req.body.email,
        password: req.body.password,
        address: req.body.address,
      },
      {
        where: { id: req.params.id },
      }
    )
    .then(function (updatedRecords) {
      if (updatedRecords == 0) {
        res.status(404).send("Not found");
        return;
      }

      users
        .findOne({
          where: { id: req.params.id },
        })
        .then((user) => {
          res.status(200).json(user);
        });
    })
    .catch((err) => res.status(500).send(err));
});

app.use("/", router);

app.listen(port, () => console.log(`Web App Up in http://localhost:${port}`));
