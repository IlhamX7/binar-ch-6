const { Router } = require("express");
const express = require("express");
const router = express.Router();
// const users = require("./users.json");

router.use(function (req, res, next) {
  console.log(req.url);
  res.setHeader("X-Masuk-Middleware", true);
  next();
});

router.get("/", function (req, res) {
  res.status(200);
  res.send("Code Challenge Chapter 5");
});

router.get("/home", function (req, res) {
  res.status(200);
  res.render("home");
});

router.get("/games", function (req, res) {
  res.status(200);
  res.render("games");
});

const users = [
  {
    id: 1,
    email: "ilham.ilham1997@yahoo.co.id",
    password: "binar",
  },
  {
    id: 2,
    email: "udin77@gmail.com",
    password: "password",
  },
  {
    id: 3,
    email: "joko_purwanto@gmail.co.id",
    password: "chapter5",
  },
];

// router.use(express.json());
// router.get("/users", function (req, res) {
//     res.json(users);
// });

router.post("/register", function (req, res) {
  const { email, password } = req.body;

  if (!email) {
    res.status(400).json({
      message: "email tidak boleh kosong",
    });
    return;
  }

  if (!password) {
    res.status(400).json({
      message: "password tidak boleh kosong",
    });
    return;
  }

  const id = users[users.length - 1].id + 1;

  const user = { id, email, password };

  users.push(user);
  res.status(200).json(user);
});

router.get("/login/:email/:password", function (req, res) {
  let login = users.find(function (login) {
    return (
      login.email === req.params.email && login.password === req.params.password
    );
  });

  if (!login) {
    res.status(401).send("Email atau password salah");
    return;
  } else {
    res.status(200).send(login);
    return;
  }
});

router.use(async (req, res, next) => {
  const error = new Error("Not found");
  error.status = 404;
  next(error);
});

router.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.send("Maaf, request yang anda minta tidak ada.");
});

module.exports = router;
